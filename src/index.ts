import http from './http';
import './mongoose';
import {server} from './graphql';

const router = http.getRouter();

router.get('/', (ctx, next) => {
	ctx.body = 'OK';
});

const PORT = 80;

(() => {
    http.listern(PORT);
    http.getApp().use(server.getMiddleware());
	console.log('Server is running on', 'localhost:' + PORT);
	console.log('GraphiQL dashboard', 'localhost:' + PORT + '/graphql');
})();


