import * as mongoose from 'mongoose';
import { OrderModel } from './entities/Order/Model';

mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('debug', true);


const mongo = {
};

const options: mongoose.ConnectionOptions = {
	auth: { user: mongo.auth.user, password: mongo.auth.password },
	dbName: mongo.auth.database,
	useNewUrlParser: true,
};

mongoose
	.connect(`mongodb://${mongo.ip}:${mongo.port}/'admin'`, options)
	.catch((e) => {
		console.error('Mongo connection error', e);
		process.exit(1);
	})
	.then(async () => {
		console.error('Mongo connection');
		// await OrderModel.deleteMany({});
		// await OrderModel.create({
		// 	name: 'Name',
		// 	shopId: 1,
		// 	products: [{ name: 'productName', count: 2 }],
		// });
		// const res = await OrderModel.find({});

		// console.log(res[0]);
		// console.log(res[0].products[0].id);

	});

export default mongoose;
