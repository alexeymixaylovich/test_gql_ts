import { IContext } from './graphql';
import { AuthChecker } from 'type-graphql';
import { types } from '@typegoose/typegoose';

enum Crm {
	Clients = 0,
	Employees = 1,
	Orders = 2,
	Tickets = 3,
	Reviews = 4,
	Incidents = 5,
	Analytics = 6,
	Promocodes = 7,
	Shops = 8,
	Tariffs = 9,
	Access = 10,
}
enum OtherRules {
	piy = 11,
}

export const Rules = {
	Crm,
	OtherRules,
};

export enum TYPES {
	read = 1,
	write = 2,
	admin = 3,
}

export type Permission = {
	rule: Crm | OtherRules;
	type: TYPES;
};

const a: Permission = {
	rule: Rules.OtherRules.piy,
	type: TYPES.read,
};

export const customAuthChecker: AuthChecker<IContext, Permission> = ({ root, args, context, info }, roles) => {
	// here we can read the user from context
	// and check his permission in the db against the `roles` argument
	// that comes from the `@Authorized` decorator, eg. ["ADMIN", "MODERATOR"]
	console.log('roles', roles);

	return true; // or false if access is denied
};
