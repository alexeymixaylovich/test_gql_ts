import * as Koa from 'koa';
import * as Router from 'koa-router';
import * as send from 'koa-send';
import * as bodyParser from 'koa-bodyparser';

export default class {
	protected app: Koa;
	protected router: Router;

	constructor() {
		this.app = new Koa();
		this.router = new Router();
	}
	getSend() {
		return send;
	}
	getRouter() {
		return this.router;
	}

    getApp() {
        return this.app;
    }

	listern(port: number) {
		this.app
			.use(bodyParser({ jsonLimit: '50mb' }))
			.use(this.router.routes())
			.use(this.router.allowedMethods());

		this.app.listen(port);
	}
}
