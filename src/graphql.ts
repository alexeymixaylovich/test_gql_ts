import { ApolloServer } from 'apollo-server-koa';
import {schema} from './entities/schema';
import { getLoaders } from './entities/loaders';



export type IContext = {
	loaders: ReturnType<typeof getLoaders>
}

export const server = new ApolloServer({
	schema,
	playground: true,
	context: ({ ctx }):IContext => {
		const { header } = ctx;
		
		return {
			loaders: getLoaders(),
			// user: getUserByToken(header['app-token']),
		};
	},
});


