import { buildSchemaSync } from 'type-graphql';
import { OrderResolver } from './Order/Resolver';
import { ShopResolver } from './Shop/Resolver';
import { SchemaDirectiveVisitor } from 'graphql-tools';

import { IsAuthDirective, PermissionDirective } from './directives';
import {customAuthChecker} from '../auth'

import * as path from 'path';

export const schema = buildSchemaSync({
	resolvers: [OrderResolver, ShopResolver],
	emitSchemaFile: path.resolve(__dirname, '../../schema.gql'),
	authChecker: customAuthChecker,
});

SchemaDirectiveVisitor.visitSchemaDirectives(schema, {
	isAuth: IsAuthDirective,
	permission: PermissionDirective,
});
