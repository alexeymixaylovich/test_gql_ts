import { prop, getModelForClass, modelOptions } from '@typegoose/typegoose';
import { Field, ObjectType, ID, Int } from 'type-graphql';
import { Shop } from '../Shop/Model';
import { Types } from 'mongoose';

@ObjectType({ description: 'Продукты' })
export class Product {
	@Field()
	@prop({ required: true })
	name!: string;

	@Field(() => Int)
	@prop({ required: true })
	count!: number;
}
@modelOptions({ schemaOptions: { collection: 'ts_gql_orders' } })
@ObjectType({ description: 'Заказы' })
export class Order {
	@Field(() => ID)
	id: Types.ObjectId;

	@Field()
	@prop({ required: true })
	name!: string;

	@Field(() => Int)
	@prop({ required: true, type: Number })
	shopId!: Shop['id'];

	@Field(() => [Product], { defaultValue: [] })
	@prop({ type: () => Product, default: [] })
	products?: Product[];
}

export const OrderModel = getModelForClass(Order);
