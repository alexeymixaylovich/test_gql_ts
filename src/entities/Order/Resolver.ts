import { Resolver, Query, Arg, ID, Mutation, FieldResolver, Root, Ctx, Args, Authorized } from 'type-graphql';
import { DocumentType } from '@typegoose/typegoose';

import { Order, Product } from './Model';
import { Shop, ShopModel } from '../Shop/Model';

import { OrderService } from './Service';
import { CreateOrderInput, ProductArgs } from './Args';

import { Container } from 'typedi';
import { IContext } from '../../graphql';
import { Permission, Rules, TYPES } from '../../auth';


@Resolver(Order)
export class OrderResolver {
	private readonly orderService: OrderService = Container.get(OrderService);
	@Authorized<Permission>({rule: Rules.Crm.Analytics, type: TYPES.read})
	@Query(() => [Order])
	async orders(): Promise<Order[]> {
		const records = await this.orderService.getAll();
		return records;
	}

	@Query(() => Order, { nullable: true })
	async order(@Arg('id', () => ID) id: Order['id']): Promise<Order> {
		return this.orderService.getById(id);
	}

	@Mutation(() => Order, { description: 'Создать заказ' })
	async orderCreate(@Arg('order') order: CreateOrderInput): Promise<Order> {
		return this.orderService.create(order);
	}

	@FieldResolver(() => Shop, { description: 'Модель Магазина' })
	async shop(
		@Root() { shopId }: DocumentType<Order>,
		@Ctx() { loaders }: IContext
	): Promise<Shop> {
		return loaders.shopById.load(shopId);
		// return ShopModel.findOne({ id: shopId });
	}

	@FieldResolver(() => Shop, { description: 'Модель Магазина' })
	products(
		@Root() { products }: DocumentType<Order>,
		@Args() { skip, take }: ProductArgs
	): Product[] {
		return products.slice(skip, skip + take);
	}
}
