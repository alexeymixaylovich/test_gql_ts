import { Order, Product } from './Model';
import { Max } from 'class-validator';
import { InputType, Field, Int, ArgsType } from 'type-graphql';

@InputType()
class ProductInput {
	@Field(() => String)
	name!: Product['name'];

	@Field(() => Int)
	count!: Product['count'];
}

@InputType()
export class CreateOrderInput {
	@Field(() => String)
	name!: Order['name'];

	@Field(() => Int)
	shopId!: Order['shopId'];

	@Field(() => [ProductInput])
	products?: ProductInput[];
}

@ArgsType()
export class ProductArgs {
	@Field(() => Int, { nullable: true, defaultValue: 0 })
    skip!: number;
    
	@Max(40)
	@Field(() => Int, { nullable: true, defaultValue: 10 })
	take!: number;
}
