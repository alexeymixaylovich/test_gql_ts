import { Service } from 'typedi';
import { OrderModel, Order } from './Model';
import { CreateOrderInput } from './Args';


Service();
export class OrderService {
	getAll() {
		return OrderModel.find({});
	}
	getById(_id: Order['id']) {
		return OrderModel.findOne({ _id });
	}
	create(order: CreateOrderInput) {
		return OrderModel.create(order);
	}
}
