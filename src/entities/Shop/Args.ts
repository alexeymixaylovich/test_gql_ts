import { Shop } from "./Model";
import { InputType, Field ,Int} from "type-graphql";

@InputType()
export class CreateShopInput implements Partial<Shop> {
  @Field(()=> Int)
  id!: number;
  
  @Field()
  name!: string;
}