import { prop, getModelForClass, modelOptions } from '@typegoose/typegoose';
import { Field, ObjectType, Int, Directive } from 'type-graphql';

// @Directive("@isAuth")
@modelOptions({ schemaOptions: { collection: 'ts_gql_shops' } })
@ObjectType({ description: 'Магазины' })
export class Shop {
	@Field(() => Int)
	@prop({ required: true })
	id!: number;

	@Field()
	@prop({ required: true })
	name!: string;
}

export const ShopModel = getModelForClass(Shop);