import { Service } from 'typedi';
import { Shop, ShopModel } from './Model';
import { CreateShopInput } from './Args';

Service();
export class ShopService {
	getAll() {
		return ShopModel.find({});
	}
	getById(id: Shop['id']) {
		return ShopModel.findOne({ id });
	}
	create(shop: CreateShopInput) {
		return ShopModel.create(shop);
	}
}

export const shopById = async (ids: Shop['id'][]): Promise<Shop[]> => {
	const arr = await ShopModel.find({ id: ids });
	return ids.map((id) => arr.find((r) => r.id === id));
};
