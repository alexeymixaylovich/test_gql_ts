import { Resolver, Query, Arg, Int, Mutation } from 'type-graphql';
import { Shop } from './Model';
import { CreateShopInput } from './Args';

import { ShopService } from './Service';
import { Container } from 'typedi';

@Resolver(Shop)
export class ShopResolver {
	private readonly shopService: ShopService = Container.get(ShopService);

	@Query(() => [Shop])
	async shops(): Promise<Shop[]> {
		const records = await this.shopService.getAll();
		return records;
	}

	@Query(() => Shop, { nullable: true })
	async shop(@Arg('id', () => Int) id: Shop['id']): Promise<Shop> {
		return this.shopService.getById(id);
	}

	@Mutation(() => Shop, { description: 'Создать магазин' })
	async shopCreate(@Arg('shop') shop: CreateShopInput): Promise<Shop> {
		return this.shopService.create(shop);
	}
}
