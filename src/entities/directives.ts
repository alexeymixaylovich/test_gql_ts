import { SchemaDirectiveVisitor } from 'graphql-tools';
import { defaultFieldResolver } from 'graphql';

enum Args {
	CTX = 2,
}



export class UpperCaseDirective extends SchemaDirectiveVisitor {
	visitFieldDefinition(field) {
		const { resolve = defaultFieldResolver } = field;
		field.resolve = async function (...args) {
			const result = await resolve.apply(this, args);
			if (typeof result === 'string') {
				const r = result.toUpperCase();
				return result.toUpperCase();
			}
			return result;
		};
	}
}

export class IsAuthDirective extends SchemaDirectiveVisitor {
	visitObject(type) {
		Object.values(type.getFields()).forEach((field) => {
			this._wrap(field, this.args.permission);
		});
	}

	visitFieldDefinition(field) {
		this._wrap(field, this.args.permission);
	}

	_wrap(field, permission) {
		const { resolve = defaultFieldResolver } = field;
		field.resolve = async function (...args) {
			console.log('_IsAuth', field.name, permission);

			const { user } = args[Args.CTX];
			if (user) {
				if (permission) {
					if (user.havePermission(permission)) return resolve.apply(this, args);
					throw new Error('permission');
				}
				return resolve.apply(this, args);
			}
			throw new Error('authenticated');
		};
	}
}

export class PermissionDirective extends SchemaDirectiveVisitor {
	visitFieldDefinition(field) {
		const { resolve = defaultFieldResolver } = field;
		const permission = this.args.permission;

		field.resolve = async function (...args) {
			const { user } = args[Args.CTX];
			// return resolve.apply(this, args);
			if (user && user.havePermission(permission)) return resolve.apply(this, args);
			throw new Error('permission');
		};
	}
}
