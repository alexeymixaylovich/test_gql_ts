import * as DataLoader from 'dataloader';

import { shopById } from './Shop/Service';
import { Shop } from './Shop/Model';

export const getLoaders = () => ({
	shopById: new DataLoader<Shop['id'], Shop>(shopById),
});
